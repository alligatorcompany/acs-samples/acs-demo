
with 
{% for source in nbr.loads -%}
    {{ '\n' if not loop.first }}{{source.rel_name}} as (
  select  {{'distinct' }}
    {# Link HK #}
    {%- for attribute in source.cbc_rel.attributes -%}
    {% set list = attribute.name.split(', ') %}
    {%- raw %}{{{% endraw %} dbt_utils.generate_surrogate_key([
      {%- for item in list -%}
         {{ ',' if not loop.first }}
         {%- raw %}'{% endraw %}{{ item }}{% raw %}'{% endraw -%}
      {%- endfor -%}  
    ]) {% raw %}}}{% endraw %} as {{attribute.alias}}_h,{{'\n    '}}  
    {%- if not loop.first %}
      {%- for item in list -%}
         {{ item }} as {{attribute.alias}}_{{ item }}{{',\n    ' if not loop.last }}
      {%- endfor -%}{{',' if not loop.last }}
    {% endif %}  
    {% endfor -%}
  from {% raw %}{{{% endraw %} ref('{{source.rel_name}}') {% raw %}}}{% endraw %} 
){{',' if not loop.last }}
{%- endfor %}

{% for source in nbr.loads -%}
select * from {{source.rel_name}}
{{' union \n' if not loop.last }}
{%- endfor %}
