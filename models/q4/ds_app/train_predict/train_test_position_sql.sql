with 

base_set as (

  select 
     {{ dbt_utils.star(from=ref('train_test_dataset_sql')) }},
     uniform(0::float, 1::float, random(42))  as RANDOM_NUMBER

  from {{ ref('train_test_dataset_sql') }}

)

select  
  {{ dbt_utils.star(from=ref('train_test_dataset_sql')) }},
  case 
    when RANDOM_NUMBER <= 0.7 then 'train'
    else 'test'
  end  as DATASET_TYPE

from base_set
