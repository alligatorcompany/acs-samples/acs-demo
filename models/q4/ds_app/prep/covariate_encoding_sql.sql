with 

source as (

  select RACE_YEAR,CIRCUIT_NAME,GRID,CONSTRUCTOR_NAME,DRIVER,DRIVERS_AGE_YEARS,DRIVER_CONFIDENCE,CONSTRUCTOR_RELAIBLITY,
         TOTAL_PIT_STOPS_PER_RACE,POSITION 
  from {{ ref('ml_data_prep_sql') }}
  where ACTIVE_DRIVER = 1 and ACTIVE_CONSTRUCTOR = 1

),

distinct_constructor as (

  select 
    array_agg(distinct CONSTRUCTOR_NAME) within group (order by CONSTRUCTOR_NAME) as distinct_constructor_name 
  
  from source

),

distinct_circuit as (

  select 
    array_agg(distinct CIRCUIT_NAME) within group (order by CIRCUIT_NAME) as distinct_circuit_name 
  
  from source

),

distinct_driver as (

  select 
    array_agg(distinct DRIVER) within group (order by DRIVER) as distinct_driver_name 
  
  from source

),

distinct_pit_stops as (

  select 
    array_agg(distinct TOTAL_PIT_STOPS_PER_RACE) within group (order by TOTAL_PIT_STOPS_PER_RACE) as distinct_pit_name 
  
  from source

)

select 
  RACE_YEAR, GRID, DRIVERS_AGE_YEARS, DRIVER_CONFIDENCE, CONSTRUCTOR_RELAIBLITY, 
  array_position(CONSTRUCTOR_NAME::variant, distinct_constructor_name) as CONSTRUCTOR_NAME,
  array_position(CIRCUIT_NAME::variant, distinct_circuit_name) as CIRCUIT_NAME,
  array_position(DRIVER::variant, distinct_driver_name) as DRIVER,
  array_position(TOTAL_PIT_STOPS_PER_RACE::variant, distinct_pit_name) as TOTAL_PIT_STOPS_PER_RACE,
  case 
      when POSITION < 4 then 1
      when POSITION > 10 then 3
      else 2
  end as POSITION_LABEL

from distinct_constructor, distinct_circuit, distinct_driver, distinct_pit_stops, source

/*
-- Test with python table --
select * from (
(select RACE_YEAR,GRID,DRIVERS_AGE_YEARS,round(DRIVER_CONFIDENCE,4),round(CONSTRUCTOR_RELAIBLITY,4),CONSTRUCTOR_NAME,CIRCUIT_NAME,DRIVER,TOTAL_PIT_STOPS_PER_RACE,POSITION_LABEL from PC_DBT_DB.FORMULA_1.COVARIATE_ENCODING

minus  

select RACE_YEAR,GRID,DRIVERS_AGE_YEARS,round(DRIVER_CONFIDENCE,4),round(CONSTRUCTOR_RELAIBLITY,4),CONSTRUCTOR_NAME,CIRCUIT_NAME,DRIVER,TOTAL_PIT_STOPS_PER_RACE,POSITION_LABEL from PC_DBT_DB.FORMULA_1.COVARIATE_ENCODING_SQL)

UNION ALL

(select RACE_YEAR,GRID,DRIVERS_AGE_YEARS,round(DRIVER_CONFIDENCE,4),round(CONSTRUCTOR_RELAIBLITY,4),CONSTRUCTOR_NAME,CIRCUIT_NAME,DRIVER,TOTAL_PIT_STOPS_PER_RACE,POSITION_LABEL from PC_DBT_DB.FORMULA_1.COVARIATE_ENCODING_SQL

minus

select RACE_YEAR,GRID,DRIVERS_AGE_YEARS,round(DRIVER_CONFIDENCE,4),round(CONSTRUCTOR_RELAIBLITY,4),CONSTRUCTOR_NAME,CIRCUIT_NAME,DRIVER,TOTAL_PIT_STOPS_PER_RACE,POSITION_LABEL from PC_DBT_DB.FORMULA_1.COVARIATE_ENCODING) 
)
order by RACE_YEAR,GRID,DRIVERS_AGE_YEARS
*/
