with 

fct_results as (

  select 
    {{ dbt_utils.star(from=ref('fct_results'), except=["POSITION", "CONSTRUCTOR_NAME", "TOTAL_PIT_STOPS_PER_RACE"]) }},
    POSITION::FLOAT as POSITION,
    zeroifnull(TOTAL_PIT_STOPS_PER_RACE) as TOTAL_PIT_STOPS_PER_RACE,
    case CONSTRUCTOR_NAME 
      when 'Force India' then 'Racing Point'
      when 'Sauber' then 'Alfa Romeo'
      when 'Lotus F1' then 'Renault'
      when 'Toro Rosso' then 'AlphaTauri'
      else CONSTRUCTOR_NAME
    end as CONSTRUCTOR_NAME

  from {{ ref('fct_results') }}
  where RACE_YEAR between 2010 and 2020

),

drivers as (

  select 
    DRIVER,
    1 - sum(DNF_FLAG)/count(DNF_FLAG) as DRIVER_CONFIDENCE

  from fct_results
  group by DRIVER

),

constructors as (

  select 
    CONSTRUCTOR_NAME,
    1 - sum(DNF_FLAG)/count(DNF_FLAG) as CONSTRUCTOR_RELAIBLITY

  from fct_results
  group by CONSTRUCTOR_NAME

), 

active as (

  select
    fct_results.*,
    (DRIVER in ('Daniel Ricciardo', 'Kevin Magnussen', 'Carlos Sainz',
               'Valtteri Bottas', 'Lance Stroll', 'George Russell',
               'Lando Norris', 'Sebastian Vettel', 'Kimi Räikkönen',
               'Charles Leclerc', 'Lewis Hamilton', 'Daniil Kvyat',
               'Max Verstappen', 'Pierre Gasly', 'Alexander Albon',
               'Sergio Pérez', 'Esteban Ocon', 'Antonio Giovinazzi',
               'Romain Grosjean','Nicholas Latifi'))::int as ACTIVE_DRIVER,
    (CONSTRUCTOR_NAME in ('Renault', 'Williams', 'McLaren', 'Ferrari', 'Mercedes',
                        'AlphaTauri', 'Racing Point', 'Alfa Romeo', 'Red Bull',
                        'Haas F1 Team'))::int as ACTIVE_CONSTRUCTOR
 
  from fct_results 

)

select 
  active.*,
  drivers.DRIVER_CONFIDENCE,
  constructors.CONSTRUCTOR_RELAIBLITY

from active
 left join drivers on active.DRIVER = drivers.DRIVER
 left join constructors on active.CONSTRUCTOR_NAME = constructors.CONSTRUCTOR_NAME

/*
-- Test with python table --
select * from (
(select driver, constructor_name, round(DRIVER_CONFIDENCE,4), round(CONSTRUCTOR_RELAIBLITY,4), ACTIVE_DRIVER, ACTIVE_CONSTRUCTOR from PC_DBT_DB.FORMULA_1.ML_DATA_PREP
minus 
select driver, constructor_name, round(DRIVER_CONFIDENCE,4), round(CONSTRUCTOR_RELAIBLITY,4), ACTIVE_DRIVER, ACTIVE_CONSTRUCTOR from PC_DBT_DB.FORMULA_1.ML_DATA_PREP_SQL)
union all
(select driver, constructor_name, round(DRIVER_CONFIDENCE,4), round(CONSTRUCTOR_RELAIBLITY,4), ACTIVE_DRIVER, ACTIVE_CONSTRUCTOR from PC_DBT_DB.FORMULA_1.ML_DATA_PREP_SQL
minus
select driver, constructor_name, round(DRIVER_CONFIDENCE,4), round(CONSTRUCTOR_RELAIBLITY,4), ACTIVE_DRIVER, ACTIVE_CONSTRUCTOR from PC_DBT_DB.FORMULA_1.ML_DATA_PREP)
)
order by constructor_name, driver
*/
