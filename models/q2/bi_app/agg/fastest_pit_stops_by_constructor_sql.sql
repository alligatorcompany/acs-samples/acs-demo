with pit_stops_joined as (

  select 
    PIT_STOP_MILLISECONDS/1000 as PIT_STOP_SECONDS,
    *
  from {{ ref('pit_stops_joined') }}
  where RACE_YEAR=2021

)

  select 
    CONSTRUCTOR_NAME,
    round(count(PIT_STOP_SECONDS),2) as COUNT,
    round(avg(PIT_STOP_SECONDS),2) as MEAN,
    round(min(PIT_STOP_SECONDS),2) as MIN,
    round(stddev(PIT_STOP_SECONDS),2) as STD,
    round(percentile_cont(0.25) within group(order by PIT_STOP_SECONDS),2) as "25%",
    round(percentile_cont(0.50) within group(order by PIT_STOP_SECONDS),2) as "50%",
    round(percentile_cont(0.75) within group(order by PIT_STOP_SECONDS),2) as "75%",
    round(max(PIT_STOP_SECONDS),2) as MAX

  from pit_stops_joined
  group by CONSTRUCTOR_NAME

/*
--
-- Test with python table --
--
-- !! one round MIN is not pass
-- 
select * from (
(select CONSTRUCTOR_NAME,COUNT,MEAN,MIN,STD,"25%","50%","75%",MAX from PC_DBT_DB.FORMULA_1.FASTEST_PIT_STOPS_BY_CONSTRUCTOR_SQL
minus 
select CONSTRUCTOR_NAME,COUNT,MEAN,MIN,STD,"25%","50%","75%",MAX from PC_DBT_DB.FORMULA_1.FASTEST_PIT_STOPS_BY_CONSTRUCTOR)
union all
(select CONSTRUCTOR_NAME,COUNT,MEAN,MIN,STD,"25%","50%","75%",MAX from PC_DBT_DB.FORMULA_1.FASTEST_PIT_STOPS_BY_CONSTRUCTOR
minus 
select CONSTRUCTOR_NAME,COUNT,MEAN,MIN,STD,"25%","50%","75%",MAX from PC_DBT_DB.FORMULA_1.FASTEST_PIT_STOPS_BY_CONSTRUCTOR_SQL)
)
order by CONSTRUCTOR_NAME
*/