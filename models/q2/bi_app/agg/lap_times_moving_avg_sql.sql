with times_years as (

  select * from {{ ref('int_lap_times_years') }}

),

group_years as (

  select 
    RACE_YEAR,
    avg(LAP_TIME_MILLISECONDS/1000) as LAP_TIME_SECONDS
     
  from times_years
  group by RACE_YEAR

)

select 
  round(LAP_TIME_SECONDS,1) as LAP_TIME_SECONDS,
  RACE_YEAR, 
  round(avg(LAP_TIME_SECONDS) OVER(
    ORDER BY RACE_YEAR ROWS BETWEEN 4 PRECEDING AND CURRENT ROW
  ), 1) as LAP_MOVING_AVG_5_YEARS
  
from group_years


/*
--
-- Test with python table --
--
-- !! NULL Values for part data
-- 
select * from (
(select RACE_YEAR, LAP_TIME_SECONDS, LAP_MOVING_AVG_5_YEARS from PC_DBT_DB.FORMULA_1.LAP_TIMES_MOVING_AVG_SQL
minus 
select RACE_YEAR, LAP_TIME_SECONDS, LAP_MOVING_AVG_5_YEARS from PC_DBT_DB.FORMULA_1.LAP_TIMES_MOVING_AVG)
union all
(select RACE_YEAR, LAP_TIME_SECONDS, LAP_MOVING_AVG_5_YEARS from PC_DBT_DB.FORMULA_1.LAP_TIMES_MOVING_AVG
minus 
select RACE_YEAR, LAP_TIME_SECONDS, LAP_MOVING_AVG_5_YEARS from PC_DBT_DB.FORMULA_1.LAP_TIMES_MOVING_AVG_SQL)
)
where RACE_YEAR >= 2000
order by RACE_YEAR
*/
