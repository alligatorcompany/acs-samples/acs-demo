with pit_stops as 
(
    select * from {{ ref('PitStops') }}
),

pit_stops_per_race as (
    select 
        raceid,
        driverid,
        stop as stop_number,
        lap,
        time as lap_time_formatted,
        duration as pit_stop_duration_seconds,
        milliseconds as pit_stop_milliseconds, 
        max(stop) over (partition by raceid,driverid) as total_pit_stops_per_race
    from pit_stops
 
)

select * from pit_stops_per_race