with lap_times as (

  select * from {{ ref('LapTimes') }}

),

races as (

  select * from {{ ref('Race') }}

),

expanded_lap_times_by_year as (
    select 
        lap_times.raceid, 
        driverid, 
        races.year as race_year,
        lap,
        milliseconds as lap_time_milliseconds 
    from lap_times
    left join races
        on lap_times.raceid = races.raceid
    where milliseconds is not null 
)

select * from expanded_lap_times_by_year