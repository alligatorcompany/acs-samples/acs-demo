with results as (

  select * from {{ ref('Result') }}

),

races as ( 

  select * from {{ ref('Race') }}

),

drivers as (

  select * from {{ ref('Driver') }}

), 

constructors as (

  select * from {{ ref('Constructor') }}
  
), 

int_results as (
    select 
      resultid,
      results.raceid,
      races.year as race_year, 
      races.round as race_round,
      circuitid,
      races.name as circuit_name,
      races.date as race_date,
      to_time(races.time) as race_time, 
      results.driverid,
      results.start_number as driver_number,
      forename ||' '|| surname as driver, 
      cast(datediff('year', dob, race_date) as int) as drivers_age_years, 
      drivers.nationality as driver_nationality,
      results.constructorid,
      constructors.name as constructor_name,
      constructors.nationality as constructor_nationality, 
      grid, 
      position,
      positiontext as position_text,
      positionorder as position_order,
      points,
      laps,
      results.time as results_time_formatted, 
      results.milliseconds results_milliseconds,
      fastestlap as fastest_lap,
      results.ranking as results_rank,
      fastestlaptime as fastest_lap_time_formatted,
      fastestlapspeed as fastest_lap_speed, 
      results.statusid,
      results.status,
      case when position is null then 1 else 0 end as dnf_flag
    from results
    left join races
      on results.raceid=races.raceid
    left join drivers 
      on results.driverid = drivers.driverid
    left join constructors 
      on results.constructorid = constructors.constructorid
    left join status
      on results.statusid = status.statusid
 )

 select * from int_results 