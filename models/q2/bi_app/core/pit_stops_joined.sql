with base_results as (

  select * from {{ ref('fct_results') }}
  
), 

pit_stops as (

  select * from {{ ref('int_pit_stops') }}
  
),

pit_stops_joined as (

    select 
        base_results.raceid,
        race_year,
        base_results.driverid,
        constructorid,
        constructor_name,
        stop_number,
        lap, 
        lap_time_formatted,
        pit_stop_duration_seconds, 
        pit_stop_milliseconds
    from base_results
    left join pit_stops
        on base_results.raceid=pit_stops.raceid and base_results.driverid=pit_stops.driverid
)
select * from pit_stops_joined