with int_results as (

  select * from {{ ref('int_results') }}

),

int_pit_stops as (
  select 
    raceid,
    driverid, 
    max(total_pit_stops_per_race) as total_pit_stops_per_race
  from {{ ref('int_pit_stops') }}
  group by 1,2
  
), 

circuits as (

  select * from {{ ref('Circuit') }}
  
), 
 
 base_results as (
     select 
        resultid,
        int_results.raceid,
        race_year, 
        race_round,
        int_results.circuitid,
        int_results.circuit_name,
        circuitref,
        location,
        country,
        lat,
        lng,
        alt,
        total_pit_stops_per_race, 
        race_date,
        race_time, 
        int_results.driverid, 
        driver, 
        driver_number,
        drivers_age_years, 
        driver_nationality,
        constructorid,
        constructor_name,
        constructor_nationality, 
        grid, 
        position,
        position_text,
        position_order,
        points,
        laps,
        results_time_formatted, 
        results_milliseconds,
        fastest_lap,
        results_rank,
        fastest_lap_time_formatted,
        fastest_lap_speed, 
        statusid,
        status,
        dnf_flag
     from int_results
     left join circuits
        on int_results.circuitid=circuits.circuitid
     left join int_pit_stops 
        on int_results.driverid=int_pit_stops.driverid and int_results.raceid=int_pit_stops.raceid
 )

select * from base_results 