with
psa as (
    select * from {{ ref('psa_insert') }}
),

order_ldts as (
    select
        payload,
        ldts,
        row_number() over (
            partition by bk order by ldts desc
        ) as latest

    from psa
    where
        source_name = 'formula1' and
        interface_name='circuits' 
),

filter_latest as (
    select payload,ldts from order_ldts where latest=1
)

select
    ldts,
    parse_json(payload):CIRCUITID::BIGINT as circuitid,
    parse_json(payload):CIRCUITREF::VARCHAR(16777216) as circuitref,
    parse_json(payload):NAME::VARCHAR(16777216) as name,
    parse_json(payload):COUNTRY::VARCHAR(16777216) as country,
    parse_json(payload):LAT::FLOAT as lat,
    parse_json(payload):LNG::FLOAT as lng,
    parse_json(payload):ALT::BIGINT as alt,
    parse_json(payload):URL::VARCHAR(16777216) as url
from filter_latest
