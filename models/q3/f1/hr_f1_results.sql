with results as (
    select
        {{ dbt_utils.star(source('formula1','results')) }}
    from {{ source('formula1','results') }}
),

status as (
    select
        {{ dbt_utils.star(source('formula1','status')) }}
    from {{ source('formula1','status') }}
)

select
    results.resultid,
    results.raceid,
    results.driverid,
    results.constructorid,
    results.number as start_number,
    results.grid,
    results.position,
    results.positiontext,
    results.positionorder,
    results.points,
    results.laps,
    results.time,
    results.milliseconds,
    results.fastestlap,
    results.rank as ranking,
    results.fastestlaptime,
    results.fastestlapspeed,
    results.statusid,
    status.status
from results inner join status on results.statusid = status.statusid
