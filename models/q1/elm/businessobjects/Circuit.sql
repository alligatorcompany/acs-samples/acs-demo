{% if target.name == 'doc' %}
with 
query_00 as (
  select * from {{ ref('H_CIRCUIT') }} 
),
query_01 as (
  select * from {{ ref('S_CIRCUIT_S_F1_R_HR_F1_CIRCUITS_U_DBT') }} 
),
finale as (
  select

    query_00.CIRCUIT_BK as CIRCUIT_BK,
    query_00.CIRCUIT_H as CIRCUIT_HK,

    query_01.CIRCUITID as CIRCUITID,
    query_01.CIRCUITREF as CIRCUITREF,
    query_01.NAME as NAME,
    query_01.LOCATION as LOCATION,
    query_01.COUNTRY as COUNTRY,
    query_01.LAT as LAT,
    query_01.LNG as LNG,
    query_01.ALT as ALT,
    query_01.URL as URL
  from 
    query_00 
    
    left join  query_01  on query_01.CIRCUIT_H = query_00.CIRCUIT_H
)
select * from finale
{%- else -%}
select * from {{ source('dvb', 'circuit_s_dvb_hub') }} 
{%- endif -%}
