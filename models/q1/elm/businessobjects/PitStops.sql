{% if target.name == 'doc' %}
with 
query_00 as (
  select * from {{ ref('H_PITSTOPS') }} 
),
query_01 as (
  select * from {{ ref('H_DRIVER') }} 
),
query_02 as (
  select * from {{ ref('H_RACE') }} 
),
query_03 as (
  select * from {{ ref('S_PITSTOPS_S_F1_R_HR_F1_PIT_STOPS_U_DBT') }} 
),
finale as (
  select

    query_00.PITSTOPS_BK as PIT_STOPS_BK,
    query_00.PITSTOPS_H as PIT_STOPS_HK,

    query_01.DRIVER_H as DRIVER_HK,

    query_02.RACE_H as RACE_HK,

    query_03.RACEID as RACEID,
    query_03.DRIVERID as DRIVERID,
    query_03.STOP as STOP,
    query_03.LAP as LAP,
    query_03.TIME as TIME,
    query_03.DURATION as DURATION,
    query_03.MILLISECONDS as MILLISECONDS
  from 
    query_00 
    
    left join  query_03  on query_03.PITSTOPS_H = query_00.PITSTOPS_H
    
    left join {{ref('L_PITSTOPS_T_DRIVER')}}  query_01_01  on query_01_01.PITSTOPS_H = query_00.PITSTOPS_H
      left join  query_01  on query_01.DRIVER_H = query_01_01.DRIVER_H
      
    left join {{ref('L_PITSTOPS_T_RACE')}}  query_02_01  on query_02_01.PITSTOPS_H = query_00.PITSTOPS_H
      left join  query_02  on query_02.RACE_H = query_02_01.RACE_H
)    
select * from finale
{%- else -%}
select * from {{ source('dvb', 'pitstops_s_dvb_hub') }} 
{%- endif -%}
