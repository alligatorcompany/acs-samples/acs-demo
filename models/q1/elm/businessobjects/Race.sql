{% if target.name == 'doc' %}
with 
query_00 as (
  select * from {{ ref('H_RACE') }} 
),
query_01 as (
  select * from {{ ref('H_CIRCUIT') }} 
),
query_02 as (
  select * from {{ ref('S_RACE_S_F1_R_HR_F1_RACES_U_DBT') }} 
),
finale as (
  select

    query_00.RACE_BK as RACE_BK,
    query_00.RACE_H as RACE_HK,

    query_01.CIRCUIT_H as CIRCUIT_HK,

    query_02.RACEID as RACEID,
    query_02.YEAR as YEAR,
    query_02.ROUND as ROUND,
    query_02.CIRCUITID as CIRCUITID,
    query_02.NAME as NAME,
    query_02.DATE as DATE,
    query_02.TIME as TIME,
    query_02.URL as URL,
    query_02.FP1_DATE as FP1_DATE,
    query_02.FP1_TIME as FP1_TIME,
    query_02.FP2_DATE as FP2_DATE,
    query_02.FP2_TIME as FP2_TIME,
    query_02.FP3_DATE as FP3_DATE,
    query_02.FP3_TIME as FP3_TIME,
    query_02.QUALI_DATE as QUALI_DATE,
    query_02.QUALI_TIME as QUALI_TIME,
    query_02.SPRINT_DATE as SPRINT_DATE,
    query_02.SPRINT_TIME as SPRINT_TIME
  from 
    query_00 
    
    left join  query_02  on query_02.RACE_H = query_00.RACE_H
    
    left join {{ref('L_CIRCUIT_T_RACE')}}  query_01_01  on query_01_01.RACE_H = query_00.RACE_H
      left join  query_01  on query_01.CIRCUIT_H = query_01_01.CIRCUIT_H
)
select * from finale
{%- else -%}
select * from {{ source('dvb', 'race_s_dvb_hub') }} 
{%- endif -%}
