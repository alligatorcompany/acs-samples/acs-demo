{% if target.name == 'doc' %}
with
query_00 as (
  select * from {{ ref('H_LAPTIMES') }} 
),
query_01 as (
  select * from {{ ref('H_RACE') }} 
),
query_02 as (
  select * from {{ ref('H_DRIVER') }} 
),
query_03 as (
  select * from {{ ref('S_LAPTIMES_S_F1_R_HR_F1_LAP_TIMES_U_DBT') }} 
),
finale as (
  select

    query_00.LAPTIMES_BK as LAP_TIMES_BK,
    query_00.LAPTIMES_H as LAP_TIMES_HK,

    query_01.RACE_H as RACE_HK,

    query_02.DRIVER_H as DRIVER_HK,

    query_03.RACEID as RACEID,
    query_03.DRIVERID as DRIVERID,
    query_03.LAP as LAP,
    query_03.POSITION as POSITION,
    query_03.TIME as TIME,
    query_03.MILLISECONDS as MILLISECONDS
  from 
    query_00 
    
    left join  query_03  on query_03.LAPTIMES_H = query_00.LAPTIMES_H
    
    left join {{ref('L_LAPTIMES_T_RACE')}}  query_01_01  on query_01_01.LAPTIMES_H = query_00.LAPTIMES_H
      left join  query_01  on query_01.RACE_H = query_01_01.RACE_H
      
    left join {{ref('L_LAPTIMES_T_DRIVER')}}  query_02_01  on query_02_01.LAPTIMES_H = query_00.LAPTIMES_H
      left join  query_02  on query_02.DRIVER_H = query_02_01.DRIVER_H
)    
select * from finale
{%- else -%}
select * from {{ source('dvb', 'laptimes_s_dvb_hub') }} 
{%- endif -%}
