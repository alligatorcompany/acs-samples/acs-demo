{% if target.name == 'doc' %}
with 
query_00 as (
  select * from {{ ref('H_DRIVER') }} 
),
query_01 as (
  select * from {{ ref('S_DRIVER_S_F1_R_HR_F1_DRIVERS_U_DBT') }} 
),
finale as (
  select

    query_00.DRIVER_BK as DRIVER_BK,
    query_00.DRIVER_H as DRIVER_HK,

    query_01.DRIVERID as DRIVERID,
    query_01.DRIVERREF as DRIVERREF,
    query_01.NUMBER as NUMBER,
    query_01.CODE as CODE,
    query_01.FORENAME as FORENAME,
    query_01.SURNAME as SURNAME,
    query_01.DOB as DOB,
    query_01.NATIONALITY as NATIONALITY,
    query_01.URL as URL
  from 
    query_00 
    left join  query_01  on query_01.DRIVER_H = query_00.DRIVER_H
)
select * from finale
{%- else -%}
select * from {{ source('dvb', 'driver_s_dvb_hub') }} 
{%- endif -%}
