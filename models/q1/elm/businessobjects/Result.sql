{% if target.name == 'doc' %}
with 
query_00 as (
  select * from {{ ref('H_RESULT') }} 
),
query_01 as (
  select * from {{ ref('H_DRIVER') }} 
),
query_02 as (
  select * from {{ ref('H_RACE') }} 
),
query_03 as (
  select * from {{ ref('H_CONSTRUCTOR') }} 
),
query_04 as (
  select * from {{ ref('S_RESULT_S_F1_R_HR_F1_RESULTS_U_DBT') }} 
),
finale as (
  select

    query_00.RESULT_BK as RESULT_BK,
    query_00.RESULT_H as RESULT_HK,

    query_01.DRIVER_H as DRIVER_HK,

    query_02.RACE_H as RACE_HK,

    query_03.CONSTRUCTOR_H as CONSTRUCTOR_HK,

    query_04.RESULTID as RESULTID,
    query_04.RACEID as RACEID,
    query_04.DRIVERID as DRIVERID,
    query_04.CONSTRUCTORID as CONSTRUCTORID,
    query_04.START_NUMBER as START_NUMBER,
    query_04.GRID as GRID,
    query_04.POSITION as POSITION,
    query_04.POSITIONTEXT as POSITIONTEXT,
    query_04.POSITIONORDER as POSITIONORDER,
    query_04.POINTS as POINTS,
    query_04.LAPS as LAPS,
    query_04.TIME as TIME,
    query_04.MILLISECONDS as MILLISECONDS,
    query_04.FASTESTLAP as FASTESTLAP,
    query_04.RANKING as RANKING,
    query_04.FASTESTLAPTIME as FASTESTLAPTIME,
    query_04.FASTESTLAPSPEED as FASTESTLAPSPEED,
    query_04.STATUSID as STATUSID,
    query_04.STATUS as STATUS
  from 
    query_00 
    
    left join  query_04  on query_04.RESULT_H = query_00.RESULT_H
    
    left join {{ref('L_DRIVER_T_RESULT')}}  query_01_01  on query_01_01.RESULT_H = query_00.RESULT_H
      left join  query_01  on query_01.DRIVER_H = query_01_01.DRIVER_H
      
    left join {{ref('L_RACE_T_RESULT')}}  query_02_01  on query_02_01.RESULT_H = query_00.RESULT_H
      left join  query_02  on query_02.RACE_H = query_02_01.RACE_H
      
    left join {{ref('L_CONSTRUCTOR_T_RESULT')}}  query_03_01  on query_03_01.RESULT_H = query_00.RESULT_H
      left join  query_03  on query_03.CONSTRUCTOR_H = query_03_01.CONSTRUCTOR_H
)
select * from finale
{%- else -%}
select * from {{ source('dvb', 'result_s_dvb_hub') }} 
{%- endif -%}
