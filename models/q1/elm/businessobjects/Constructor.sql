{% if target.name == 'doc' %}
with 
query_00 as (
  select * from {{ ref('H_CONSTRUCTOR') }} 
),
query_01 as (
  select * from {{ ref('S_CONSTRUCTOR_S_F1_R_HR_F1_CONSTRUCTORS_U_DBT') }} 
),
finale as (
  select

    query_00.CONSTRUCTOR_BK as CONSTRUCTOR_BK,
    query_00.CONSTRUCTOR_H as CONSTRUCTOR_HK,

    query_01.CONSTRUCTORID as CONSTRUCTORID,
    query_01.CONSTRUCTORREF as CONSTRUCTORREF,
    query_01.NAME as NAME,
    query_01.NATIONALITY as NATIONALITY,
    query_01.URL as URL
  from 
    query_00 
    
    left join  query_01  on query_01.CONSTRUCTOR_H = query_00.CONSTRUCTOR_H
)
select * from finale
{%- else -%}
select * from {{ source('dvb', 'constructor_s_dvb_hub') }} 
{%- endif -%}
