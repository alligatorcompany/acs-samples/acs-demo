
with 
hr_f1_drivers as (
  select  
    {{ dbt.concat(['DRIVERID']) }} as driver_bk,
    {{ dbt_utils.generate_surrogate_key(['DRIVERID']) }} as driver_h
  from {{ ref('hr_f1_drivers') }} 
)

select * from hr_f1_drivers

