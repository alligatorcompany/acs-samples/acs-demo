
with 
hr_f1_pit_stops as (
  select  
    {{ dbt.concat(['RACEID','DRIVERID','STOP']) }} as pitstops_bk,
    {{ dbt_utils.generate_surrogate_key(['RACEID','DRIVERID','STOP']) }} as pitstops_h
  from {{ ref('hr_f1_pit_stops') }} 
)

select * from hr_f1_pit_stops

