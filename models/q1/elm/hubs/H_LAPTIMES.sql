
with 
hr_f1_lap_times as (
  select  
    {{ dbt.concat(['RACEID','DRIVERID','LAP']) }} as laptimes_bk,
    {{ dbt_utils.generate_surrogate_key(['RACEID','DRIVERID','LAP']) }} as laptimes_h
  from {{ ref('hr_f1_lap_times') }} 
)

select * from hr_f1_lap_times

