
with 
hr_f1_constructors as (
  select  
    {{ dbt.concat(['CONSTRUCTORID']) }} as constructor_bk,
    {{ dbt_utils.generate_surrogate_key(['CONSTRUCTORID']) }} as constructor_h
  from {{ ref('hr_f1_constructors') }} 
)

select * from hr_f1_constructors

