
with 
hr_f1_races as (
  select  
    {{ dbt.concat(['RACEID']) }} as race_bk,
    {{ dbt_utils.generate_surrogate_key(['RACEID']) }} as race_h
  from {{ ref('hr_f1_races') }} 
)

select * from hr_f1_races

