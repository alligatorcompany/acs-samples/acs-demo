
with 
hr_f1_results as (
  select  
    {{ dbt.concat(['RESULTID']) }} as result_bk,
    {{ dbt_utils.generate_surrogate_key(['RESULTID']) }} as result_h
  from {{ ref('hr_f1_results') }} 
)

select * from hr_f1_results

