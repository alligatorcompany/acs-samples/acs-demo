
with 
hr_f1_circuits as (
  select  
    {{ dbt.concat(['CIRCUITID']) }} as circuit_bk,
    {{ dbt_utils.generate_surrogate_key(['CIRCUITID']) }} as circuit_h
  from {{ ref('hr_f1_circuits') }} 
)

select * from hr_f1_circuits

