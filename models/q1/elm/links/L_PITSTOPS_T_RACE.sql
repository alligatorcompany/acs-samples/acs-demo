
with 
hr_f1_pit_stops as (
  select  distinct
    {{ dbt_utils.generate_surrogate_key(['RACEID','DRIVERID','STOP','RACEID']) }} as pitstops_t_race_h,
      
    {{ dbt_utils.generate_surrogate_key(['RACEID','DRIVERID','STOP']) }} as pitstops_h,
    RACEID as pitstops_RACEID,
    DRIVERID as pitstops_DRIVERID,
    STOP as pitstops_STOP,
      
    {{ dbt_utils.generate_surrogate_key(['RACEID']) }} as race_h,
    RACEID as race_RACEID
      
    from {{ ref('hr_f1_pit_stops') }} 
)

select * from hr_f1_pit_stops
