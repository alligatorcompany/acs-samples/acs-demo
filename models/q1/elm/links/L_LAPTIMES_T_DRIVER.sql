
with 
hr_f1_lap_times as (
  select  distinct
    {{ dbt_utils.generate_surrogate_key(['RACEID','DRIVERID','LAP','DRIVERID']) }} as laptimes_t_driver_h,
      
    {{ dbt_utils.generate_surrogate_key(['RACEID','DRIVERID','LAP']) }} as laptimes_h,
    RACEID as laptimes_RACEID,
    DRIVERID as laptimes_DRIVERID,
    LAP as laptimes_LAP,
      
    {{ dbt_utils.generate_surrogate_key(['DRIVERID']) }} as driver_h,
    DRIVERID as driver_DRIVERID
      
    from {{ ref('hr_f1_lap_times') }} 
)

select * from hr_f1_lap_times
