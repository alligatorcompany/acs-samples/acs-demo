
with 
hr_f1_results as (
  select  distinct
    {{ dbt_utils.generate_surrogate_key(['CONSTRUCTORID','RESULTID']) }} as constructor_t_result_h,
      
    {{ dbt_utils.generate_surrogate_key(['CONSTRUCTORID']) }} as constructor_h,
    CONSTRUCTORID as constructor_CONSTRUCTORID,
      
    {{ dbt_utils.generate_surrogate_key(['RESULTID']) }} as result_h,
    RESULTID as result_RESULTID
      
    from {{ ref('hr_f1_results') }} 
)

select * from hr_f1_results
