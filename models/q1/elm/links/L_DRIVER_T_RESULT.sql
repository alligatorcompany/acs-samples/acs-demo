
with 
hr_f1_results as (
  select  distinct
    {{ dbt_utils.generate_surrogate_key(['DRIVERID','RESULTID']) }} as driver_t_result_h,
      
    {{ dbt_utils.generate_surrogate_key(['DRIVERID']) }} as driver_h,
    DRIVERID as driver_DRIVERID,
      
    {{ dbt_utils.generate_surrogate_key(['RESULTID']) }} as result_h,
    RESULTID as result_RESULTID
      
    from {{ ref('hr_f1_results') }} 
)

select * from hr_f1_results
