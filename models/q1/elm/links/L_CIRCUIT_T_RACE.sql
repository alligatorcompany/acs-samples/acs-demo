
with 
hr_f1_races as (
  select  distinct
    {{ dbt_utils.generate_surrogate_key(['CIRCUITID','RACEID']) }} as circuit_t_race_h,
      
    {{ dbt_utils.generate_surrogate_key(['CIRCUITID']) }} as circuit_h,
    CIRCUITID as circuit_CIRCUITID,
      
    {{ dbt_utils.generate_surrogate_key(['RACEID']) }} as race_h,
    RACEID as race_RACEID
      
    from {{ ref('hr_f1_races') }} 
)

select * from hr_f1_races
