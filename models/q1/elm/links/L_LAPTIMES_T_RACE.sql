
with 
hr_f1_lap_times as (
  select  distinct
    {{ dbt_utils.generate_surrogate_key(['RACEID','DRIVERID','LAP','RACEID']) }} as laptimes_t_race_h,
      
    {{ dbt_utils.generate_surrogate_key(['RACEID','DRIVERID','LAP']) }} as laptimes_h,
    RACEID as laptimes_RACEID,
    DRIVERID as laptimes_DRIVERID,
    LAP as laptimes_LAP,
      
    {{ dbt_utils.generate_surrogate_key(['RACEID']) }} as race_h,
    RACEID as race_RACEID
      
    from {{ ref('hr_f1_lap_times') }} 
)

select * from hr_f1_lap_times
