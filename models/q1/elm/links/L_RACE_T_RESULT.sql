
with 
hr_f1_results as (
  select  distinct
    {{ dbt_utils.generate_surrogate_key(['RACEID','RESULTID']) }} as race_t_result_h,
      
    {{ dbt_utils.generate_surrogate_key(['RACEID']) }} as race_h,
    RACEID as race_RACEID,
      
    {{ dbt_utils.generate_surrogate_key(['RESULTID']) }} as result_h,
    RESULTID as result_RESULTID
      
    from {{ ref('hr_f1_results') }} 
)

select * from hr_f1_results
