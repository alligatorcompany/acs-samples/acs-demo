
with 
hr_f1_lap_times as (
  select  
    {{ dbt_utils.generate_surrogate_key(['RACEID', 'DRIVERID', 'LAP'])}} as laptimes_h,
    {{ dbt_utils.generate_surrogate_key(['POSITION', 'TIME', 'MILLISECONDS'])}} as hkdiff,
    RACEID,
    DRIVERID,
    LAP,
    POSITION,
    TIME,
    MILLISECONDS
  from {{ ref('hr_f1_lap_times') }} 
)
select * from hr_f1_lap_times
