
with 
hr_f1_pit_stops as (
  select  
    {{ dbt_utils.generate_surrogate_key(['RACEID', 'DRIVERID', 'STOP'])}} as pitstops_h,
    {{ dbt_utils.generate_surrogate_key(['LAP', 'TIME', 'DURATION', 'MILLISECONDS'])}} as hkdiff,
    RACEID,
    DRIVERID,
    STOP,
    LAP,
    TIME,
    DURATION,
    MILLISECONDS
  from {{ ref('hr_f1_pit_stops') }} 
)
select * from hr_f1_pit_stops
