
with 
hr_f1_drivers as (
  select  
    {{ dbt_utils.generate_surrogate_key(['DRIVERID'])}} as driver_h,
    {{ dbt_utils.generate_surrogate_key(['DRIVERREF', 'NUMBER', 'CODE', 'FORENAME', 'SURNAME', 'DOB', 'NATIONALITY', 'URL'])}} as hkdiff,
    DRIVERID,
    DRIVERREF,
    NUMBER,
    CODE,
    FORENAME,
    SURNAME,
    DOB,
    NATIONALITY,
    URL
  from {{ ref('hr_f1_drivers') }} 
)
select * from hr_f1_drivers
