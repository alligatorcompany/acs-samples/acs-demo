
with 
hr_f1_races as (
  select  
    {{ dbt_utils.generate_surrogate_key(['RACEID'])}} as race_h,
    {{ dbt_utils.generate_surrogate_key(['YEAR', 'ROUND', 'CIRCUITID', 'NAME', 'DATE', 'TIME', 'URL', 'FP1_DATE', 'FP1_TIME', 'FP2_DATE', 'FP2_TIME', 'FP3_DATE', 'FP3_TIME', 'QUALI_DATE', 'QUALI_TIME', 'SPRINT_DATE', 'SPRINT_TIME'])}} as hkdiff,
    RACEID,
    YEAR,
    ROUND,
    CIRCUITID,
    NAME,
    DATE,
    TIME,
    URL,
    FP1_DATE,
    FP1_TIME,
    FP2_DATE,
    FP2_TIME,
    FP3_DATE,
    FP3_TIME,
    QUALI_DATE,
    QUALI_TIME,
    SPRINT_DATE,
    SPRINT_TIME
  from {{ ref('hr_f1_races') }} 
)
select * from hr_f1_races
