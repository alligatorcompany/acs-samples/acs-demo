
with 
hr_f1_constructors as (
  select  
    {{ dbt_utils.generate_surrogate_key(['CONSTRUCTORID'])}} as constructor_h,
    {{ dbt_utils.generate_surrogate_key(['CONSTRUCTORREF', 'NAME', 'NATIONALITY', 'URL'])}} as hkdiff,
    CONSTRUCTORID,
    CONSTRUCTORREF,
    NAME,
    NATIONALITY,
    URL
  from {{ ref('hr_f1_constructors') }} 
)
select * from hr_f1_constructors
