
with 
hr_f1_circuits as (
  select  
    {{ dbt_utils.generate_surrogate_key(['CIRCUITID'])}} as circuit_h,
    {{ dbt_utils.generate_surrogate_key(['CIRCUITREF', 'NAME', 'LOCATION', 'COUNTRY', 'LAT', 'LNG', 'ALT', 'URL'])}} as hkdiff,
    CIRCUITID,
    CIRCUITREF,
    NAME,
    LOCATION,
    COUNTRY,
    LAT,
    LNG,
    ALT,
    URL
  from {{ ref('hr_f1_circuits') }} 
)
select * from hr_f1_circuits
