
with 
hr_f1_results as (
  select  
    {{ dbt_utils.generate_surrogate_key(['RESULTID'])}} as result_h,
    {{ dbt_utils.generate_surrogate_key(['RACEID', 'DRIVERID', 'CONSTRUCTORID', 'START_NUMBER', 'GRID', 'POSITION', 'POSITIONTEXT', 'POSITIONORDER', 'POINTS', 'LAPS', 'TIME', 'MILLISECONDS', 'FASTESTLAP', 'RANKING', 'FASTESTLAPTIME', 'FASTESTLAPSPEED', 'STATUSID', 'STATUS'])}} as hkdiff,
    RESULTID,
    RACEID,
    DRIVERID,
    CONSTRUCTORID,
    START_NUMBER,
    GRID,
    POSITION,
    POSITIONTEXT,
    POSITIONORDER,
    POINTS,
    LAPS,
    TIME,
    MILLISECONDS,
    FASTESTLAP,
    RANKING,
    FASTESTLAPTIME,
    FASTESTLAPSPEED,
    STATUSID,
    STATUS
  from {{ ref('hr_f1_results') }} 
)
select * from hr_f1_results
